package com.exo.coor.correction.exceptions;

public class UsernamePasswordInvalidException extends RuntimeException {

    public UsernamePasswordInvalidException(){ super("Username/password invalid"); }

}
