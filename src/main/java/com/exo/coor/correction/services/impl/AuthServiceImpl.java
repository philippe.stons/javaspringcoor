package com.exo.coor.correction.services.impl;

import com.exo.coor.correction.configs.jwt.JwtTokenProvider;
import com.exo.coor.correction.mappers.UserMapper;
import com.exo.coor.correction.models.dtos.UserDTO;
import com.exo.coor.correction.models.entities.User;
import com.exo.coor.correction.models.forms.User.UserLoginForm;
import com.exo.coor.correction.models.forms.User.UserRegisterForm;
import com.exo.coor.correction.repositories.UserRepository;
import com.exo.coor.correction.services.AuthService;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AuthServiceImpl implements AuthService {
    private final PasswordEncoder encoder;
    private final UserRepository repository;
    private final UserMapper mapper;
    private final JwtTokenProvider provider;
    private AuthenticationManager manager;

    public AuthServiceImpl(PasswordEncoder encoder, UserRepository repository, UserMapper mapper,
            JwtTokenProvider provider, AuthenticationManager manager) {
        this.encoder = encoder;
        this.repository = repository;
        this.mapper = mapper;
        this.provider = provider;
        this.manager = manager;
    }

    @Override
    public UserDTO login(UserLoginForm form) {
        try {
            User u = repository.findByUsername(form.getUsername())
                .orElseThrow(() -> new IllegalArgumentException("User does not exist!"));

            manager.authenticate(new UsernamePasswordAuthenticationToken(form.getUsername(), form.getPassword()));
            UserDTO dto = mapper.entityToDTO(u);
            dto.setToken(provider.createToken(u));

            return dto;
        } catch (Exception e) {
            log.info(e.getMessage());
            throw new IllegalArgumentException("Incorrect credentials!");
        }
    }

    @Override
    public UserDTO register(UserRegisterForm form) {
        User u = new User();

        u = mapper.formToEntity(form, u);
        UserDTO dto = mapper.entityToDTO(repository.save(u));
        dto.setToken(provider.createToken(u));

        return dto;
    }
    
}
