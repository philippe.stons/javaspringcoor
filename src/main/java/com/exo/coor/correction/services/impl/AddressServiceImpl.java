package com.exo.coor.correction.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.exo.coor.correction.mappers.AddressMapper;
import com.exo.coor.correction.models.dtos.AddressDTO;
import com.exo.coor.correction.models.entities.Address;
import com.exo.coor.correction.models.forms.Address.AddressForm;
import com.exo.coor.correction.repositories.AddressRepository;
import com.exo.coor.correction.repositories.UserRepository;
import com.exo.coor.correction.services.BaseService;

import org.springframework.stereotype.Service;

@Service
public class AddressServiceImpl implements BaseService<AddressDTO, AddressForm, Long> {
    private final AddressRepository repository;
    private final UserRepository userRepo;
    private final AddressMapper mapper;

    public AddressServiceImpl(AddressRepository repository, UserRepository userRepo, AddressMapper mapper) {
        this.repository = repository;
        this.userRepo = userRepo;
        this.mapper = mapper;
    }

    @Override
    public List<AddressDTO> getAll() {
        return repository.findAll().stream()
            .map(mapper::entityToDto)
            .collect(Collectors.toList());
    }

    @Override
    public AddressDTO getOneById(Long id) {
        return mapper.entityToDto(repository.findById(id).orElse(null));
    }

    @Override
    public AddressDTO insert(AddressForm form) {
        Address a = new Address();
        a = mapper.formToEntity(form, a);
        return mapper.entityToDto(repository.save(a));
    }

    @Override
    public AddressDTO update(AddressForm form, Long id) {
        Address address = repository.findById(id).orElse(null);
        address = mapper.formToEntity(form, address);
        return mapper.entityToDto(repository.save(address));
    }

    @Override
    public Long delete(Long id) {
        Address address = repository.findById(id).orElse(null);
        repository.delete(address);
        return address.getId();
    }
    
}
