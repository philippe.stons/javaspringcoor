package com.exo.coor.correction.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.exo.coor.correction.mappers.UserMapper;
import com.exo.coor.correction.models.dtos.UserDTO;
import com.exo.coor.correction.models.dtos.UserProductDTO;
import com.exo.coor.correction.models.entities.User;
import com.exo.coor.correction.models.forms.User.UserInsertForm;
import com.exo.coor.correction.models.forms.User.UserUpdateBasketForm;
import com.exo.coor.correction.models.forms.User.UserUpdateForm;
import com.exo.coor.correction.repositories.UserRepository;
import com.exo.coor.correction.services.UserBaseService;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserDetailsServiceImpl implements UserBaseService {
    private final UserRepository repository;
    private final UserMapper mapper;
    
    public UserDetailsServiceImpl(UserRepository repository, UserMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return this.repository.findByUsername(username)
            .orElseThrow(() -> new IllegalArgumentException("User does not exist!"));
    }

    @Override
    public List<UserDTO> getAll() {
        return this.repository.findAll().stream()
            .map(this.mapper::entityToDTO)
            .collect(Collectors.toList());
    }

    @Override
    public UserDTO getOneByID(Long id) {
        return this.mapper.entityToDTO(this.repository.findById(id)
            .orElse(null));
    }

    public List<UserProductDTO> getUserBasket()
    {
        String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User current = this.repository.findByUsername(username).orElse(null);

        if(current != null)
        {
            return this.mapper.extractBasket(current);
        }
        throw new IllegalAccessError("Access denied!");
    }
    
    public UserDTO insert(UserInsertForm form)
    {
        User u = new User();

        u = mapper.formToEntity(form, u);

        return mapper.entityToDTO(repository.save(u));
    }

    public UserDTO update(UserUpdateForm form, Long id)
    {
        User u = this.repository.findById(id)
            .orElseThrow(() -> new IllegalArgumentException("User does not exist!"));

        if(mapper.isCurrentUserOrAdmin(u.getUsername()))
        {
            u = mapper.formToEntity(form, u);
        }

        return mapper.entityToDTO(repository.save(u));
    }

    public UserDTO updateCurrent(UserUpdateForm form)
    {
        String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User current = this.repository.findByUsername(username).orElse(null);

        if(current != null)
        {
            current = mapper.formToEntity(form, current);
        }

        return mapper.entityToDTO(repository.save(current));
    }

    public List<UserProductDTO> update(UserUpdateBasketForm form)
    {
        String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User current = this.repository.findByUsername(username).orElse(null);

        if(current != null)
        {
            current = mapper.formToEntity(form, current);
        }

        return mapper.extractBasket(repository.save(current));
    }

    public Long delete(Long id)
    {
        User user = repository.findById(id).orElse(null);

        if(mapper.isCurrentUserOrAdmin(user.getUsername()))
        {
            repository.delete(user);
        }

        return user.getId();
    }
}
