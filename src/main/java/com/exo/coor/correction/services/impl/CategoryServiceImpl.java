package com.exo.coor.correction.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.exo.coor.correction.mappers.CategoryMapper;
import com.exo.coor.correction.models.dtos.CategoryDTO;
import com.exo.coor.correction.models.entities.Category;
import com.exo.coor.correction.models.forms.Category.CategoryForm;
import com.exo.coor.correction.repositories.CategoryRepository;
import com.exo.coor.correction.services.BaseService;

import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl implements BaseService<CategoryDTO, CategoryForm, Long> {
    private final CategoryRepository repository;
    private final CategoryMapper mapper;
    
    public CategoryServiceImpl(CategoryRepository repository, CategoryMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public List<CategoryDTO> getAll() {
        return this.repository.findAll().stream()
            .map(this.mapper::entityToDto)
            .collect(Collectors.toList());
    }

    @Override
    public CategoryDTO getOneById(Long id) {
        return mapper.entityToDto(repository.findById(id).orElse(null));
    }

    @Override
    public CategoryDTO insert(CategoryForm form) {
        Category c = this.mapper.formToEntity(form, new Category());
        
        return mapper.entityToDto(this.repository.save(c));
    }

    @Override
    public CategoryDTO update(CategoryForm form, Long id) {
        Category c = repository.findById(id).orElse(null);
        this.mapper.formToEntity(form, c);
        return mapper.entityToDto(this.repository.save(c));
    }

    @Override
    public Long delete(Long id) {
        Category c = repository.findById(id).orElse(null);
        repository.delete(c);
        return c.getId();
    }
    
}
