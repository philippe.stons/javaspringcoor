package com.exo.coor.correction.services;

import java.util.List;

public interface BaseService<TDTO, TFORM, TID> {
    List<TDTO> getAll();
    TDTO getOneById(TID id);
    TDTO insert(TFORM form);
    TDTO update(TFORM form, TID id);
    TID delete(TID id);
}
