package com.exo.coor.correction.services;

import java.util.List;

import com.exo.coor.correction.models.dtos.UserDTO;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserBaseService extends UserDetailsService {
    List<UserDTO> getAll();
    UserDTO getOneByID(Long id);
}
