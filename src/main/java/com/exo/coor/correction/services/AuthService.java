package com.exo.coor.correction.services;

import com.exo.coor.correction.models.dtos.UserDTO;
import com.exo.coor.correction.models.forms.User.UserLoginForm;
import com.exo.coor.correction.models.forms.User.UserRegisterForm;

public interface AuthService {
    UserDTO login(UserLoginForm form);
    UserDTO register(UserRegisterForm form);
}
