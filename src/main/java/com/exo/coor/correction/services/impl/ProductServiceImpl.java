package com.exo.coor.correction.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.exo.coor.correction.mappers.ProductMapper;
import com.exo.coor.correction.models.dtos.ProductDTO;
import com.exo.coor.correction.models.entities.Product;
import com.exo.coor.correction.models.forms.Product.ProductForm;
import com.exo.coor.correction.repositories.ProductRepository;
import com.exo.coor.correction.services.BaseService;

import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements BaseService<ProductDTO, ProductForm, Long> {
    private final ProductRepository repository;
    private final ProductMapper mapper;

    public ProductServiceImpl(ProductRepository repository, ProductMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public List<ProductDTO> getAll() {
        return this.repository.findAll().stream()
            .map(this.mapper::entityToDto)
            .collect(Collectors.toList());
    }

    @Override
    public ProductDTO getOneById(Long id) {
        return mapper.entityToDto(repository.findById(id).orElse(null));
    }

    @Override
    public ProductDTO insert(ProductForm form) {
        Product c = this.mapper.formToEntity(form, new Product());
        
        return mapper.entityToDto(this.repository.save(c));
    }

    @Override
    public ProductDTO update(ProductForm form, Long id) {
        Product c = repository.findById(id).orElse(null);
        this.mapper.formToEntity(form, c);
        return mapper.entityToDto(this.repository.save(c));
    }

    @Override
    public Long delete(Long id) {
        Product c = repository.findById(id).orElse(null);
        repository.delete(c);
        return c.getId();
    }
    
}
