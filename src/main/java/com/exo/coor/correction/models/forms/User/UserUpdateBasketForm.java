package com.exo.coor.correction.models.forms.User;

import java.util.List;

import com.exo.coor.correction.models.dtos.UserProductDTO;

import org.springframework.validation.annotation.Validated;

import lombok.Data;

@Data
@Validated
public class UserUpdateBasketForm {
    private List<UserProductDTO> basket;
}
