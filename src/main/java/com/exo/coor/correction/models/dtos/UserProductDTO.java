package com.exo.coor.correction.models.dtos;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserProductDTO {
    private Long id;
    private Long productId;
    private String label;
    private String categoryLabel;
    private Integer quantity;
}
