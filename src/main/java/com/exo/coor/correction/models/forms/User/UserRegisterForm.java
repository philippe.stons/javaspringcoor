package com.exo.coor.correction.models.forms.User;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;

import com.exo.coor.correction.models.forms.Address.AddressForm;

import org.springframework.validation.annotation.Validated;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Validated
@AllArgsConstructor
@NoArgsConstructor
public class UserRegisterForm {
    @Pattern(regexp = "^[a-zA-Z0-9]{5,20}")
    private String username;

    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,32}$")
    private String password;

    @Email
    private String email;

    private AddressForm address;
}
