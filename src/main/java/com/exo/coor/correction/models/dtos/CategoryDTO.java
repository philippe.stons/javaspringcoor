package com.exo.coor.correction.models.dtos;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CategoryDTO {
    private Long id;
    private String label;
}
