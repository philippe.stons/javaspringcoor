package com.exo.coor.correction.models.dtos;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder

public class UserDTO {
    private Long id;
    private String username;
    private String email;
    private List<String> roles;
    private AddressDTO address;
    private List<UserProductDTO> basket;

    private String token;

    private boolean accountNonExpired;
    private boolean accountNonLocked;
    private boolean credentialsNonExpired;
    private boolean enabled;
}
