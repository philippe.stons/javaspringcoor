package com.exo.coor.correction.models.forms.User;

import javax.validation.constraints.Email;

import com.exo.coor.correction.models.forms.Address.AddressForm;

import org.springframework.validation.annotation.Validated;

import lombok.Data;

@Data
@Validated
public class UserUpdateForm {
    @Email
    private String email;

    private AddressForm address;
}
