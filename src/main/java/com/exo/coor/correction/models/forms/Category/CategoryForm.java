package com.exo.coor.correction.models.forms.Category;

import org.hibernate.validator.constraints.Length;
import org.springframework.validation.annotation.Validated;

import lombok.Data;

@Data
@Validated
public class CategoryForm {
    @Length(min = 2, max = 50)
    private String label;
}
