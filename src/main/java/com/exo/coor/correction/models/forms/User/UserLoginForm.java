package com.exo.coor.correction.models.forms.User;

import org.springframework.validation.annotation.Validated;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Validated
@AllArgsConstructor
@NoArgsConstructor
public class UserLoginForm {
    private String username;
    private String password;
}
