package com.exo.coor.correction.models.dtos;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AddressDTO {
    private Long id;
    private String street;
    private Integer number;
    private String city;
    private String state;
    private Integer zipCode;
    private String country;
}
