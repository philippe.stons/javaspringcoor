package com.exo.coor.correction.models.dtos;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProductDTO {
    private Long id;
    private String label;
    private Double price;
    private Integer quantity;
    private CategoryDTO category;
}
