package com.exo.coor.correction.models.forms.Address;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.Length;
import org.springframework.validation.annotation.Validated;

import lombok.Data;

@Data
@Validated
public class AddressForm {
    @Length(min = 4, max = 50)
    private String street;
    
    @Min(0)
    private Integer number;
    
    @Length(min = 4, max = 50)
    private String city;

    @Length(min = 4, max = 50)
    private String state;

    @Min(1000)
    @Max(9999)
    private Integer zipCode;

    @Length(min = 4, max = 50)
    private String country;
}
