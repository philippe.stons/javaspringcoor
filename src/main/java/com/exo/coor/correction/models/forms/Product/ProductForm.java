package com.exo.coor.correction.models.forms.Product;

import javax.validation.constraints.Min;

import com.exo.coor.correction.models.entities.Category;

import org.hibernate.validator.constraints.Length;
import org.springframework.validation.annotation.Validated;

import lombok.Data;

@Data
@Validated
public class ProductForm {
    @Length(min = 2, max = 50)
    private String label;
    @Min(0)
    private Double price;
    @Min(0)
    private Integer quantity;
    private Category category;
}
