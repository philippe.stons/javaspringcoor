package com.exo.coor.correction.init;

import java.util.Arrays;
import java.util.List;

import com.exo.coor.correction.models.entities.Address;
import com.exo.coor.correction.models.entities.Category;
import com.exo.coor.correction.models.entities.Product;
import com.exo.coor.correction.models.entities.User;
import com.exo.coor.correction.models.entities.UserProducts;
import com.exo.coor.correction.repositories.AddressRepository;
import com.exo.coor.correction.repositories.CategoryRepository;
import com.exo.coor.correction.repositories.ProductRepository;
import com.exo.coor.correction.repositories.UserProductsRepository;
import com.exo.coor.correction.repositories.UserRepository;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class DatabaseInit implements InitializingBean
{
    private final UserRepository userRepo;
    private final AddressRepository addressRepo;
    private final PasswordEncoder encoder;
    private final CategoryRepository catRepo;
    private final ProductRepository prodRepo;
    private final UserProductsRepository userProdRepo;

    public DatabaseInit(UserRepository userRepo, AddressRepository addressRepo,
            CategoryRepository catRepo, ProductRepository prodRepo,
            UserProductsRepository userProdRepo,
            PasswordEncoder encoder) {
        this.userRepo = userRepo;
        this.addressRepo = addressRepo;
        this.encoder = encoder;
        this.catRepo = catRepo;
        this.prodRepo = prodRepo;
        this.userProdRepo = userProdRepo;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        log.info(">>> !! INITIALIZE DATABASE !! <<<");

        List<Category> categories = Arrays.asList(
            Category.builder()
                .label("Fruits")        // 0
                .build(),
            Category.builder()
                .label("Surgelé")       // 1
                .build(),
            Category.builder()
                .label("Boulangerie")   // 2
                .build(),
            Category.builder()
                .label("Boucherie")     // 3
                .build()
        );
        categories.forEach(this.catRepo::save);

        List<Product> products = Arrays.asList(
            Product.builder()
                .label("Tomate")
                .category(categories.get(0))    // 0
                .price(5.25)
                .quantity(10)
                .build(),
            Product.builder()
                .label("Poivrons")
                .category(categories.get(0))    // 1
                .price(5.25)
                .quantity(10)
                .build(),
            Product.builder()
                .label("Grand carré blanc")
                .category(categories.get(2))    // 2
                .price(5.25)
                .quantity(10)
                .build(),
            Product.builder()
                .label("Pizza")
                .category(categories.get(1))    // 3
                .price(5.25)
                .quantity(10)
                .build(),
            Product.builder()
                .label("Haché porc/boeuf")
                .category(categories.get(3))    // 4
                .price(5.25)
                .quantity(10)
                .build()
        );

        products.forEach(this.prodRepo::save);

        List<Address> address = Arrays.asList(
            Address.builder()
                .street("Rue du test")
                .number(404)
                .state("Nevada")
                .city("Testing")
                .zipCode(1111)
                .country("Belgium")
                .build(),
            Address.builder()
                .street("Rue du dev")
                .number(500)
                .state("Nevada")
                .city("Testing")
                .zipCode(1111)
                .country("Belgium")
                .build(),
            Address.builder()
                .street("Rue de la prod")
                .number(200)
                .state("Nevada")
                .city("Testing")
                .zipCode(1111)
                .country("Belgium")
                .build()
        );

        List<User> users = Arrays.asList(
            User.builder()
                .username("admin")
                .password(this.encoder.encode("admin"))
                .email("admin@admin.com")
                .roles(List.of("ADMIN", "USER"))
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .address(address.get(0))
                .build(),
            User.builder()
                .username("user")
                .email("user@user.com")
                .password(this.encoder.encode("user"))
                .roles(List.of("USER"))
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .address(address.get(2))
                .build(),
            User.builder()
                .username("test")
                .email("test@test.com")
                .password(this.encoder.encode("test"))
                .roles(List.of("USER"))
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .address(address.get(1))
                .build()
        );

        users.forEach(this.userRepo::save);

        List<UserProducts> baskets = Arrays.asList(
            UserProducts.builder()
                .user(users.get(0))
                .product(products.get(0))
                .quantity(2)
                .build(),
            UserProducts.builder()
                .user(users.get(0))
                .product(products.get(4))
                .quantity(1)
                .build(),
            UserProducts.builder()
                .user(users.get(0))
                .product(products.get(2))
                .quantity(2)
                .build()
        );
        baskets.forEach(this.userProdRepo::save);

        log.info(">>> !! DATABASE INITIALIZED !! <<<");
    }
}