package com.exo.coor.correction.controller;

import java.util.List;

import javax.validation.Valid;

import com.exo.coor.correction.exceptions.UsernamePasswordInvalidException;
import com.exo.coor.correction.models.dtos.UserDTO;
import com.exo.coor.correction.models.dtos.UserProductDTO;
import com.exo.coor.correction.models.forms.User.UserInsertForm;
import com.exo.coor.correction.models.forms.User.UserUpdateBasketForm;
import com.exo.coor.correction.models.forms.User.UserUpdateForm;
import com.exo.coor.correction.services.impl.UserDetailsServiceImpl;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

@RestController
@RequestMapping(path = "/user")
public class UserController {
    private final UserDetailsServiceImpl service;

    public UserController(UserDetailsServiceImpl service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<List<UserDTO>> getList()
    {
//        throw new UsernamePasswordInvalidException();
        return ResponseEntity.ok(this.service.getAll());
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<UserDTO> getOne(@PathVariable(name = "id") Long id)
    {
        return ResponseEntity.ok(this.service.getOneByID(id));
    }

    @GetMapping(path = "/basket")
    public ResponseEntity<List<UserProductDTO>> getBasket()
    {
        return ResponseEntity.ok(this.service.getUserBasket());
    }

    @PutMapping(path = "/{id}")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public ResponseEntity<UserDTO> update(@PathVariable(name = "id") Long id, @RequestBody @Valid UserUpdateForm form)
    {
        return ResponseEntity.ok(this.service.update(form, id));
    }

    @PutMapping()
    public ResponseEntity<UserDTO> updateCurrent(@RequestBody @Valid UserUpdateForm form)
    {
        return ResponseEntity.ok(this.service.updateCurrent(form));
    }

    @PutMapping(path = "/basket/")
    public ResponseEntity<List<UserProductDTO>> update(@RequestBody @Valid UserUpdateBasketForm form)
    {
        return ResponseEntity.ok(this.service.update(form));
    }

    @PostMapping
    public ResponseEntity<UserDTO> insert(@RequestBody @Valid UserInsertForm form)
    {
        return ResponseEntity.ok(this.service.insert(form));
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Long> delete(@PathVariable(name = "id") Long id)
    {
        return ResponseEntity.ok(this.service.delete(id));
    }
}
