package com.exo.coor.correction.controller;

import java.util.List;

import javax.validation.Valid;

import com.exo.coor.correction.models.dtos.AddressDTO;
import com.exo.coor.correction.models.forms.Address.AddressForm;
import com.exo.coor.correction.services.impl.AddressServiceImpl;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AddressController {
    private final AddressServiceImpl service;

    public AddressController(AddressServiceImpl service)
    {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<List<AddressDTO>> getList()
    {
        return ResponseEntity.ok(this.service.getAll());
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<AddressDTO> getOne(@PathVariable(name = "id") Long id)
    {
        return ResponseEntity.ok(this.service.getOneById(id));
    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<AddressDTO> update(@PathVariable(name = "id") Long id, @RequestBody @Valid AddressForm form)
    {
        return ResponseEntity.ok(this.service.update(form, id));
    }

    @PostMapping
    public ResponseEntity<AddressDTO> insert(@RequestBody @Valid AddressForm form)
    {
        return ResponseEntity.ok(this.service.insert(form));
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Long> delete(@PathVariable(name = "id") Long id)
    {
        return ResponseEntity.ok(this.service.delete(id));
    }
}
