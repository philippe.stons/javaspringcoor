package com.exo.coor.correction.controller;

import java.util.List;

import javax.validation.Valid;

import com.exo.coor.correction.models.dtos.ProductDTO;
import com.exo.coor.correction.models.forms.Product.ProductForm;
import com.exo.coor.correction.services.impl.ProductServiceImpl;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/product")
public class ProductController {
    private final ProductServiceImpl service;

    public ProductController(ProductServiceImpl service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<List<ProductDTO>> getList()
    {
        return ResponseEntity.ok(this.service.getAll());
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<ProductDTO> getOne(@PathVariable(name = "id") Long id)
    {
        return ResponseEntity.ok(this.service.getOneById(id));
    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<ProductDTO> update(@PathVariable(name = "id") Long id, @RequestBody @Valid ProductForm form)
    {
        return ResponseEntity.ok(this.service.update(form, id));
    }

    @PostMapping
    public ResponseEntity<ProductDTO> insert(@RequestBody @Valid ProductForm form)
    {
        return ResponseEntity.ok(this.service.insert(form));
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Long> delete(@PathVariable(name = "id") Long id)
    {
        return ResponseEntity.ok(this.service.delete(id));
    }
}
