package com.exo.coor.correction.controller;

import javax.validation.Valid;

import com.exo.coor.correction.models.dtos.UserDTO;
import com.exo.coor.correction.models.forms.User.UserLoginForm;
import com.exo.coor.correction.models.forms.User.UserRegisterForm;
import com.exo.coor.correction.services.AuthService;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthController {
    private AuthService service;

    public AuthController(AuthService service) {
        this.service = service;
    }
    
    @PostMapping("/login")
    public ResponseEntity<UserDTO> login(@RequestBody @Valid UserLoginForm form)
    {
        return ResponseEntity.ok(service.login(form));
    }

    @PostMapping("/register")
    public ResponseEntity<UserDTO> register(@RequestBody @Valid UserRegisterForm form)
    {
        return ResponseEntity.ok(service.register(form));
    }
}
