package com.exo.coor.correction.repositories;

import com.exo.coor.correction.models.entities.Address;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Long> {
}
