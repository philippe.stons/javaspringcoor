package com.exo.coor.correction.repositories;

import com.exo.coor.correction.models.entities.UserProducts;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserProductsRepository extends JpaRepository<UserProducts, Long> {
}
