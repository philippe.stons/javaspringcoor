package com.exo.coor.correction.mappers;

import com.exo.coor.correction.models.dtos.AddressDTO;
import com.exo.coor.correction.models.entities.Address;
import com.exo.coor.correction.models.forms.Address.AddressForm;

import org.springframework.stereotype.Service;

@Service
public class AddressMapper implements BaseMapper<AddressDTO, AddressForm, Address> {

    @Override
    public Address formToEntity(AddressForm form, Address address) {
        if(form != null)
        {
            address.setStreet(form.getStreet());
            address.setNumber(form.getNumber());
            address.setCity(form.getCity());
            address.setState(form.getState());
            address.setZipCode(form.getZipCode());
            address.setCountry(form.getCountry());
        }
        return address;
    }

    @Override
    public AddressDTO entityToDto(Address entity) {
        if(entity != null && entity.getId() > 0)
        {
            return AddressDTO.builder()
                .id(entity.getId())
                .street(entity.getStreet())
                .number(entity.getNumber())
                .city(entity.getCity())
                .state(entity.getState())
                .zipCode(entity.getZipCode())
                .country(entity.getCountry())
                .build();
        }
        return null;
    }

    @Override
    public Address dtoToEntity(AddressDTO dto) {
        Address a = new Address();

        if(dto != null && dto.getId() > 0)
        {
            a.setId(dto.getId());
            a.setStreet(dto.getStreet());
            a.setNumber(dto.getNumber());
            a.setCity(dto.getCity());
            a.setState(dto.getState());
            a.setZipCode(dto.getZipCode());
            a.setCountry(dto.getCountry());
        }

        return a;
    }
    
}
