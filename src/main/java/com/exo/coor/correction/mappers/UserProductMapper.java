package com.exo.coor.correction.mappers;

import java.util.ArrayList;
import java.util.List;

import com.exo.coor.correction.models.dtos.UserProductDTO;
import com.exo.coor.correction.models.entities.User;
import com.exo.coor.correction.models.entities.UserProducts;
import com.exo.coor.correction.repositories.ProductRepository;

import org.springframework.stereotype.Service;

@Service
public class UserProductMapper {
    private final ProductRepository repository;

    public UserProductMapper(ProductRepository repository) {
        this.repository = repository;
    }

    public UserProductDTO entityToDto(UserProducts entity)
    {
        if(entity != null && entity.getId() > 0)
        {
            return UserProductDTO.builder()
                .id(entity.getId())
                .productId(entity.getProduct().getId())
                .label(entity.getProduct().getLabel())
                .categoryLabel(entity.getProduct().getCategory().getLabel())
                .quantity(entity.getQuantity())
                .build();
        }
        return null;
    }

    public List<UserProducts> basketToEntity(List<UserProductDTO> basket, User user)
    {
        List<UserProducts> bkt = new ArrayList<>();

        for(UserProductDTO item: basket)
        {
            UserProducts entry = new UserProducts();
            entry.setId(item.getId());
            entry.setQuantity(item.getQuantity());
            entry.setUser(user);
            entry.setProduct(repository.findById(item.getProductId())
                .orElseThrow(() -> new IllegalArgumentException("Product does not exist!")));
            bkt.add(entry);
        }
        
        return bkt;
    }
}
