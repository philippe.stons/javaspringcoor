package com.exo.coor.correction.mappers;

import com.exo.coor.correction.models.dtos.CategoryDTO;
import com.exo.coor.correction.models.entities.Category;
import com.exo.coor.correction.models.forms.Category.CategoryForm;

import org.springframework.stereotype.Service;

@Service
public class CategoryMapper implements BaseMapper<CategoryDTO, CategoryForm, Category> {

    @Override
    public Category formToEntity(CategoryForm form, Category entity) {
        entity.setLabel(form.getLabel());
        return entity;
    }

    @Override
    public CategoryDTO entityToDto(Category entity) {
        if(entity != null)
        {
            return CategoryDTO.builder()
                .id(entity.getId())
                .label(entity.getLabel())
                .build();
        }
        return null;
    }

    @Override
    public Category dtoToEntity(CategoryDTO dto) {
        Category c = new Category();

        if(dto != null && dto.getId() > 0)
        {
            c.setId(dto.getId());
            c.setLabel(dto.getLabel());
        }

        return c;
    }
    
}
