package com.exo.coor.correction.mappers;

import com.exo.coor.correction.models.dtos.ProductDTO;
import com.exo.coor.correction.models.entities.Product;
import com.exo.coor.correction.models.forms.Product.ProductForm;

import org.springframework.stereotype.Service;

@Service
public class ProductMapper implements BaseMapper<ProductDTO, ProductForm, Product> {
    private final CategoryMapper catMapper;

    public ProductMapper(CategoryMapper catMapper) {
        this.catMapper = catMapper;
    }

    @Override
    public Product formToEntity(ProductForm form, Product entity) {

        entity.setCategory(form.getCategory());
        entity.setLabel(form.getLabel());
        entity.setPrice(form.getPrice());
        entity.setQuantity(form.getQuantity());

        return entity;
    }

    @Override
    public ProductDTO entityToDto(Product entity) {
        if(entity != null && entity.getId() > 0)
        {
            return ProductDTO.builder()
                .id(entity.getId())
                .category(catMapper.entityToDto(entity.getCategory()))
                .label(entity.getLabel())
                .price(entity.getPrice())
                .quantity(entity.getQuantity())
                .build();
        }
        return null;
    }

    @Override
    public Product dtoToEntity(ProductDTO dto) {
        if(dto != null && dto.getId() > 0)
        {
            return Product.builder()
                .id(dto.getId())
                .label(dto.getLabel())
                .category(catMapper.dtoToEntity(dto.getCategory()))
                .price(dto.getPrice())
                .quantity(dto.getQuantity())
                .build();
        }
        return null;
    }
    
}
