package com.exo.coor.correction.mappers;

public interface BaseMapper<TDTO, TFORM, TENTITY> {
    public TENTITY formToEntity(TFORM form, TENTITY entity);
    public TDTO entityToDto(TENTITY entity);
    public TENTITY dtoToEntity(TDTO dto);
}