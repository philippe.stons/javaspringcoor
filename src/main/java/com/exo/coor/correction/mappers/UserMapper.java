package com.exo.coor.correction.mappers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.exo.coor.correction.models.dtos.UserDTO;
import com.exo.coor.correction.models.dtos.UserProductDTO;
import com.exo.coor.correction.models.entities.Address;
import com.exo.coor.correction.models.entities.User;
import com.exo.coor.correction.models.forms.User.UserInsertForm;
import com.exo.coor.correction.models.forms.User.UserRegisterForm;
import com.exo.coor.correction.models.forms.User.UserUpdateBasketForm;
import com.exo.coor.correction.models.forms.User.UserUpdateForm;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserMapper {
    private final PasswordEncoder encoder;
    private final AddressMapper addressMapper;
    private final UserProductMapper userProductMapper;

    public UserMapper(PasswordEncoder encoder, AddressMapper addressMapper,
        UserProductMapper userProductMapper) {
        this.encoder = encoder;
        this.addressMapper = addressMapper;
        this.userProductMapper = userProductMapper;
    }

    public UserDTO entityToDTO(User user)
    {
        if(user != null)
        {
            return UserDTO.builder()
                .id(user.getId())
                .username(user.getUsername())
                .email(user.getEmail())
                .roles(user.getRoles())
                .address(
                    isCurrentUserOrAdmin(user.getUsername()) ?
                    addressMapper.entityToDto(user.getAddress())
                    : null
                    )
                .accountNonExpired(user.isAccountNonExpired())
                .accountNonLocked(user.isAccountNonLocked())
                .credentialsNonExpired(user.isCredentialsNonExpired())
                .enabled(user.isEnabled())
                .build();
        }

        return null;
    }

    public User formToEntity(UserRegisterForm form, User user)
    {
        if(form != null)
        {
            user.setUsername(form.getUsername());
            user.setPassword(this.encoder.encode(form.getPassword()));
            user.setEmail(form.getEmail());
            user.setRoles(List.of("USER"));
            Address a = new Address();
            user.setAddress(addressMapper.formToEntity(form.getAddress(), a));
            user.setAccountNonExpired(true);
            user.setAccountNonLocked(true);
            user.setCredentialsNonExpired(true);
            user.setEnabled(true);
        }

        return user;
    }

    public User formToEntity(UserUpdateBasketForm form, User user)
    {
        if(form != null)
        {
            user.setBasket(userProductMapper.basketToEntity(form.getBasket(), user));
        }

        return user;
    }

    public User formToEntity(UserInsertForm form, User user)
    {
        if(form != null)
        {
            user.setUsername(form.getUsername());
            user.setPassword(this.encoder.encode(form.getPassword()));
            user.setEmail(form.getEmail());
            user.setRoles(form.getRoles());
            Address a = new Address();
            user.setAddress(addressMapper.formToEntity(form.getAddress(), a));
            user.setAccountNonExpired(true);
            user.setAccountNonLocked(true);
            user.setCredentialsNonExpired(true);
            user.setEnabled(true);
        }

        return user;
    }

    public User formToEntity(UserUpdateForm form, User user)
    {
        if(form != null)
        {
            user.setEmail(form.getEmail());
            addressMapper.formToEntity(form.getAddress(), user.getAddress());
        }

        return user;
    }

    public List<UserProductDTO> extractBasket(User user)
    {
        return user.getBasket() != null ?
            user.getBasket().stream()
                .map(userProductMapper::entityToDto)
                .collect(Collectors.toList())
            : new ArrayList<>();
    }

    public boolean isCurrentUserOrAdmin(String username)
    {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String current = "";
        if(principal instanceof String)
        {
            current = (String) principal;
        } else if(principal instanceof User)
        {
            current = ((User) principal).getUsername();
        }

        if(current != null)
        {
            return current == username || SecurityContextHolder.getContext().getAuthentication()
                .getAuthorities()
                .stream().anyMatch(a -> a.getAuthority().equals("ADMIN"));
        }

        return false;
    }

    public boolean isCurrentUser(String username)
    {
        String current = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if(current != null)
        {
            return current == username;
        }

        return false;
    }
}
